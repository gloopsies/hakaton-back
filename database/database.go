package database

import (
	"fmt"

	"database/sql"

	_ "github.com/lib/pq"

	. "back/config"
)

var (
	db *sql.DB
)

func DBConnect() error {
	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		Config.Db.Host, Config.Db.Port, Config.Db.Username, Config.Db.Password, Config.Db.Name)

	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		return err
	}

	err = db.Ping()
	if err != nil {
		return err
	}

	return nil
}

func UserExists(username string) (bool, error) {
	var err error
	var count int

	row := db.QueryRow("SELECT count(id) from users where username = $1", username)
	err = row.Scan(&count)

	return count > 0, err
}

func UserRegister(username string, password []byte) error {
	var err error

	_, err = db.Exec("INSERT INTO users(username, password) VALUES ($1, $2)", username, password)

	return err
}

func UserGetPassword(username string) ([]byte, error) {
	var err error
	var password []byte

	row := db.QueryRow("SELECT get_password($1)", username)
	err = row.Scan(&password)

	return password, err
}

type UserT struct {
	Username string `json:"username"`
	Admin    bool   `json:"admin"`
}

func UserGet(username string) (UserT, error) {
	var user UserT

	row := db.QueryRow("SELECT username, admin from users where username=$1", username)
	err := row.Scan(&user.Username, &user.Admin)

	return user, err
}

func UserUpdate(oldUsername string, newUsername string, newPassword []byte) error {
	var err error
	_, err = db.Exec(
		"UPDATE users SET username = $2, password = $3 WHERE username = $1",
		oldUsername,
		newUsername,
		newPassword,
	)

	return err
}
