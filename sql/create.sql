set timezone to 'UTC';

CREATE TABLE Users
(
    ID       BIGSERIAL PRIMARY KEY,
    username VARCHAR UNIQUE NOT NULL,
    password BYTEA          NOT NULL,
    admin    BOOL                    DEFAULT false NOT NULL,
    joined   TIMESTAMP      NOT NULL DEFAULT now()
);

CREATE INDEX username ON Users (username);

CREATE FUNCTION get_password(username VARCHAR)
    RETURNS BYTEA
    LANGUAGE plpgsql
AS
$$
DECLARE
    pass   BYTEA;
    number INT;
BEGIN
    SELECT count(Users.username)
    INTO number
    FROM Users
    WHERE Users.username = get_password.username;

    IF number != 1 THEN RETURN NULL; END IF;

    SELECT password
    INTO pass
    FROM Users
    WHERE Users.username = get_password.username;

    RETURN pass;
END;
$$;

--- Random

DELETE
FROM users
WHERE id > 0;
SELECT *
FROM users;

SELECT count(id) from users where username = 'user1';