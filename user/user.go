package user

import (
	"crypto"
	"regexp"

	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"net/http"

	"golang.org/x/crypto/bcrypt"

	"git.radojevic.rs/toolbox/error"

	"back/database"

	. "back/config"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Admin    bool   `json:"admin"`
}

const (
	userRegex = "^[A-Za-z0-9-_]{5,}$"
	passRegex = "^[A-Za-z0-9-_.!?@#$%^&*()+\"',/:;<=>`{|}~]{6,}$"
)

func (user *User) passwordEncrypt() ([]byte, *error.Error) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, error.NewInternal("Error generating secure hash", err)
	}

	encrypted, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, Config.Key.Public, hashed, nil)
	if err != nil {
		return nil, error.NewInternal("Error encrypting password hash", err)
	}

	return encrypted, nil
}

func (user *User) passwordCheck(encrypted []byte) *error.Error {
	decrypted, err := Config.Key.Private.Decrypt(nil, encrypted, &rsa.OAEPOptions{Hash: crypto.SHA256})
	if err != nil {
		return error.NewInternal("Error decrypting password hash", err)
	}

	if err := bcrypt.CompareHashAndPassword(decrypted, []byte(user.Password)); err != nil {
		e := error.New(http.StatusUnauthorized, "Wrong username/password combination")
		return e
	}

	return nil
}

func (user *User) Check() *error.Error {
	if uOk, err := regexp.MatchString(userRegex, user.Username); err != nil || !uOk {
		return error.New(http.StatusBadRequest, "Wrong username format")
	}

	if pOk, err := regexp.MatchString(passRegex, user.Password); err != nil || !pOk {
		return error.New(http.StatusBadRequest, "Wrong password format")
	}

	return nil
}

func (user *User) ShouldExist() *error.Error {
	exists, err := database.UserExists(user.Username)
	if err != nil {
		return error.NewInternal("Error reading from database", err)
	}

	if !exists {
		return error.New(http.StatusBadRequest, "User doesn't exist")
	}

	return nil
}

func (user *User) ShouldNotExist() *error.Error {
	exists, err := database.UserExists(user.Username)
	if err != nil {
		return error.NewInternal("Error reading from database", err)
	}

	if exists {
		return error.New(http.StatusBadRequest, "User already exists")
	}

	return nil
}

func (user *User) Register() *error.Error {
	if err := user.ShouldNotExist(); err != nil {
		return err
	}

	encryptedPassword, e := user.passwordEncrypt()
	if e != nil {
		return e
	}

	if err := database.UserRegister(user.Username, encryptedPassword); err != nil {
		return error.NewInternal("Error writing to database", err)
	}

	return nil
}

func (user *User) Login() *error.Error {
	if err := user.ShouldExist(); err != nil {
		return err
	}

	encryptedPassword, err := database.UserGetPassword(user.Username)
	if err != nil {
		return error.NewInternal("Error reading from database", err)
	}

	e := user.passwordCheck(encryptedPassword)
	return e
}

func Get(username string) (*User, *error.Error) {
	exists, err := database.UserExists(username)
	if err != nil {
		return nil, error.NewInternal("Error reading from database", err)
	}

	if !exists {
		return nil, error.New(http.StatusBadRequest, "User doesn't exist")
	}

	u, err := database.UserGet(username)
	if err != nil {
		return nil, error.NewInternal("Error reading user data", err)
	}

	user := User{
		Username: u.Username,
		Admin:    u.Admin,
	}

	return &user, nil
}

func (user *User) Update(new *User) *error.Error {
	encryptedPassword, err := new.passwordEncrypt()
	if err != nil {
		return err
	}

	if err := database.UserUpdate(user.Username, new.Username, encryptedPassword); err != nil {
		return error.NewInternal("Error writing to database", err)
	}

	return nil
}
