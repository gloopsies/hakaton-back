package main

import (
	"back/database"
	"git.radojevic.rs/toolbox/error"
	"git.radojevic.rs/toolbox/flags"

	"back/routes"

	. "back/config"
	. "back/redis-connection"
)

func main() {
	if _, err := flags.Parse(&Config); err != nil {
		error.NewInternal("Error parsing command line arguments", err)
		return
	}

	if err := Config.Read(); err != nil {
		error.NewInternal("Error reading config file", err)
		return
	}
	error.Init(Config.Debug, Config.LogFile)

	if _, err := flags.Parse(&Config); err != nil {
		error.NewInternal("Error parsing command line arguments", err)
		return
	}

	if err := database.DBConnect(); err != nil {
		error.NewInternal("Error connecting to the PostgreSQL database", err)
		return
	}

	if err := RedisInit(); err != nil {
		error.NewInternal("Error connecting to the Redis database", err)
		return
	}

	routes.Router()
}
