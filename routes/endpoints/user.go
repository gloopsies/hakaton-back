package endpoints

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Login(context *gin.Context) {
	context.String(http.StatusOK, "Logged in")
}

func Register(context *gin.Context) {
	context.String(http.StatusOK, "Registered")
}

func Update(context *gin.Context) {
	context.String(http.StatusOK, "Updated")
}

func LoggedIn(context *gin.Context) {
	context.String(http.StatusOK, "Logged in")
}

func Get(context *gin.Context) {
	context.String(http.StatusOK, "Got user")
}
