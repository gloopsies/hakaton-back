module back

go 1.16

require (
	git.radojevic.rs/toolbox/error v0.1.4
	git.radojevic.rs/toolbox/flags v0.1.2
	github.com/gin-gonic/gin v1.7.7
	github.com/go-redis/redis/v8 v8.11.4
	github.com/google/uuid v1.3.0
	github.com/lib/pq v1.10.4
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
