package routes

import (
	"back/routes/endpoints"
	"net/http"

	"github.com/gin-gonic/gin"

	"git.radojevic.rs/toolbox/error"

	. "back/config"
)

func Router() *error.Error {
	if Config.Debug {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	server := gin.Default()
	server.Use(cors)
	if err := server.SetTrustedProxies(nil); err != nil {
		return error.NewWithError(1, "Error starting server", err)
	}

	s := server.Group("/api")
	{
		s.POST("/login", logOut, login, newSession, endpoints.Login)
		s.POST("/register", logOut, register, newSession, endpoints.Register)
		s.POST("/update", loggedIn, newSession, update, endpoints.Update)
		s.POST("/loggedin", loggedIn, newSession, endpoints.LoggedIn)
		s.POST("/get", get, endpoints.Get)
	}

	server.NoRoute(func(c *gin.Context) {
		c.String(http.StatusNotFound, "route not found")
	})

	if err := server.Run(":" + Config.Port); err != nil {
		return error.NewInternal("Error running server", err)
	}
	return nil
}
