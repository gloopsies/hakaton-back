package services

import (
	"crypto"
	"fmt"
	"strings"
	"time"

	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"net/http"

	"github.com/google/uuid"

	"git.radojevic.rs/toolbox/error"

	. "back/config"
	. "back/redis-connection"
)

func NewSession(username string) (string, *error.Error) {
	sessionUUID, err := uuid.NewRandom()
	if err != nil {
		return "", error.NewInternal("Error generating unique session ID", err)
	}

	sessionHash := sha256.New()
	sessionHash.Write([]byte(sessionUUID.String()))
	sessionHashSum := sessionHash.Sum(nil)
	signature, err := rsa.SignPSS(rand.Reader, Config.Key.Private, crypto.SHA256, sessionHashSum, nil)
	if err != nil {
		return "", error.NewInternal("Error signing session", err)
	}

	status := RedisClient.SetEX(RedisContext, "session: "+base64.StdEncoding.EncodeToString(sessionHashSum), username, 24*time.Hour)
	err = status.Err()
	if err != nil {
		return "", error.NewInternal("Error writing session to session database", err)
	}

	session := fmt.Sprintf("%s$%s", base64.StdEncoding.EncodeToString(sessionHashSum), base64.StdEncoding.EncodeToString(signature))
	return session, nil
}

func VerifySession(session string) (string, *error.Error) {
	s := strings.Split(session, "$")
	if len(s) != 2 {
		return "", error.New(http.StatusBadRequest, "Incorrect session format")
	}

	sessionHash, err := base64.StdEncoding.DecodeString(s[0])
	if err != nil {
		return "", error.New(http.StatusBadRequest, "Wrong session format")
	}
	signature, err := base64.StdEncoding.DecodeString(s[1])
	if err != nil {
		return "", error.New(http.StatusBadRequest, "Wrong session signature format")
	}

	err = rsa.VerifyPSS(Config.Key.Public, crypto.SHA256, sessionHash, signature, nil)
	if err != nil {
		return "", error.New(http.StatusUnauthorized, "Incorrect session signature")
	}

	result := RedisClient.Get(RedisContext, "session: "+s[0])
	username, err := result.Result()
	if err != nil {
		return "", error.New(http.StatusUnauthorized, "Error retrieving data from session database")
	}

	return username, nil
}

func RemoveSession(session string) *error.Error {
	s := strings.Split(session, "$")
	sessionHash, err := base64.StdEncoding.DecodeString(s[0])
	if err != nil {
		return error.New(http.StatusBadRequest, "Wrong session id format")
	}

	RedisClient.Del(RedisContext, "session: "+base64.StdEncoding.EncodeToString(sessionHash))

	return nil
}
