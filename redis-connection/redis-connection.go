package redis_connection

import (
	"context"
	"errors"
	"github.com/go-redis/redis/v8"

	. "back/config"
)

var (
	RedisContext = context.Background()
	RedisClient  *redis.Client
)

func RedisInit() error {
	RedisClient = redis.NewClient(&redis.Options{
		Addr:     Config.Redis.Host,
		Password: Config.Redis.Password,
	})

	if pong, err := RedisClient.Ping(RedisContext).Result(); err != nil {
		return err
	} else if pong != "PONG" {
		return errors.New("wrong redis server response")
	}

	return nil
}
